#include<bits/stdc++.h>
#define MAXN 1000
using namespace std;
pair<int,int> points[MAXN];
int a[MAXN];
int b[MAXN];

int main(){
    int n;
    ifstream input;
    input.open("input3copy.txt");
    if (input.is_open()) {
        input>>n;
        int counter=0;
        while (!input.eof()) {
            input >> points[counter].first>>points[counter].second;
            a[counter] = counter;
            counter++;
        }
    }
    input.close();
    double minim = 10000000;

    do {
        double dist=0;
        int x1=points[a[0]].first , y1=points[a[0]].second;
        int x2,y2;
        for(int i=0 ; i<n ; i++){
            x2 = points[a[i]].first;
            y2 = points[a[i]].second;
            dist+=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
            cout<<"("<<x2<<","<<y2<<")"<<" -> ";
           // cout<<" *"<<sqrt(pow(x2-x1,2)+pow(y2-y1,2))<<"* ";
            x1=x2;
            y1=y2;
        }
        x2=points[a[0]].first, y2=points[a[0]].second;
        dist+=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
        cout<<"("<<x2<<","<<y2<<")" << " = " << dist <<endl;
        if(dist<minim){
            minim = dist;
            for(int i=0 ; i<n ; i++){
                b[i] = a[i];
            }
        }
    } while (next_permutation(a,a+n) );
    cout<<endl<<endl;
    for(int i=0 ; i<n ; i++){
        cout<<"("<<points[b[i]].first<<","<<points[b[i]].second<<")"<<" -> ";
    }
    cout<<"("<<points[b[0]].first<<","<<points[b[0]].second<<")"<<" = "<<minim<<endl;
    return 0;
}
