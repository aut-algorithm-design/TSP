#include<bits/stdc++.h>
#define MAXN 1000008
using namespace std;
pair<int,int> points[MAXN];
int a[MAXN];
bool marked[MAXN];

int main(){
    int n;
    cin>>n;
    srand (time(NULL));
    clock_t begin_time = clock();
    for(int i=0 ; i<n ; i++){
        points[i].first = rand()%201-100;
        points[i].second = rand()%201-100;
    }

    double total_dist =0 ;
    double dist;



    int minim_index=0;
    int x1,y1,x2,y2;
    a[0] = 0;
    for(int i=1 ; i<n ; i++){
        dist=1000000;
        x1=points[minim_index].first , y1=points[minim_index].second;
        marked[minim_index] = true;
        int last = minim_index;
    //    cout<<"("<<x1<<","<<y1<<")"<<" -> ";
        double new_dist;
        for(int j=0 ; j<n ; j++){
            if(last!=j && marked[j]==false){
                x2 = points[j].first , y2 = points[j].second;
                new_dist=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
                if(new_dist < dist){
                    minim_index = j;
                    dist = new_dist;
                }
            }
        }
        a[i]=minim_index;
        total_dist += dist;
    }
    x1=points[minim_index].first , y1=points[minim_index].second;
    x2=points[0].first , y2 = points[0].second;
    total_dist+=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
  //  cout<<"("<<x1<<","<<y1<<")"<<" -> ";
   // cout<<"("<<points[0].first<<","<<points[0].second<<")"<<" = "<<total_dist<<endl;
    cout<<"Time = "<< float( clock () - begin_time ) /  CLOCKS_PER_SEC   <<endl;

    return 0;
}

